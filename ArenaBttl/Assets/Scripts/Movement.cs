﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    public float speed = 5f;
    float ruchPoPrzekatnej = 0.7f;

    Rigidbody2D rb;
    //public JoystickMovement joystick;
    public Joystick joystick;

    float horizontal;
    float vertical;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        horizontal = joystick.Horizontal;
        vertical = joystick.Vertical;
    }
    private void FixedUpdate()
    {
        if (horizontal != 0 && vertical != 0)
        {
            horizontal *= ruchPoPrzekatnej;
            vertical *= ruchPoPrzekatnej;
        }
        rb.velocity = new Vector2(horizontal * speed, vertical * speed);


        //rb.velocity = new Vector2(horizontal, vertical);

        //if(joystick.joystickVec.y != 0)
        //{
        //    rb.velocity = new Vector2(joystick.joystickVec.x * speed, joystick.joystickVec.y * speed);
        //}
        //else
        //{
        //    rb.velocity = Vector2.zero;
        //}
    }
}
