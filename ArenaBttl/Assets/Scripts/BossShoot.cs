﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossShoot : MonoBehaviour
{
    public Transform punktWystrzalu;
    public GameObject testKula;
    public float kulaForce = 5f;
    public bool bossShootMode = true;

    GameObject kula;
    Rigidbody2D rb;

    float timer = 0;
    float coIleStrzal = 0.5f;

    void Update()
    {
        if (bossShootMode)
        {
            if(timer<0)
            {
                Shoot();
                timer = coIleStrzal;
            }
            timer -= Time.deltaTime;
        }
    }
    void Shoot()
    {
        kula = Instantiate(testKula, punktWystrzalu.position, punktWystrzalu.rotation);
        rb = kula.GetComponent<Rigidbody2D>();
        rb.AddForce(-punktWystrzalu.up * kulaForce, ForceMode2D.Impulse);
    }
}
