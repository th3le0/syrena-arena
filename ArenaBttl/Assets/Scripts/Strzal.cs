﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Strzal : MonoBehaviour
{
    public GameObject efektKolizji;

    GameObject efekt;
    private void OnCollisionEnter2D(Collision2D collision)
    {
        Instantiate(efektKolizji, transform.position, Quaternion.identity);
        Destroy(efekt, 5f);
        Destroy(gameObject);

    }
}
